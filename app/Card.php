<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Card
 * @package App
 * @property string $name
 * @property int $converted_mana_cost
 * @property string $mana_cost
 * @property string $rules_text
 * @property string $flavor_text
 * @property string $original_text
 * @property Set $set
 * @property string $layout
 * @property array $names
 * @property array $colors
 * @property array $color_identity
 * @property string $type
 * @property array $types
 * @property array $superTypes
 * @property array $subTypes
 * @property string $rarity
 * @property string $original_type
 * @property array $legalities
 * @property bool $reserved
 * @property string $border
 * @property string $artist
 * @property string $power
 * @property string $toughness
 * @property int $loyalty
 * @property string $number
 * @property array $foreign_names
 * @property string $multiverseid
 * @property array $variations
 * @property string $watermark
 * @property string $hand
 * @property string $life
 * @property Carbon $release_date
 */
class Card extends Model
{
    protected $table = 'cards';

    public $incrementing = false;

    protected $fillable = [
        'name',
        'names',
        'mana_cost',
        'converted_mana_cost',
        'colors',
        'color_identity',
        'type',
        'types',
        'supertypes',
        'subtypes',
        'rarity',
        'rules_text',
        'flavor_text',
        'artist',
        'number',
        'power',
        'toughness',
        'loyalty',
        'multiverseid',
        'variations',
        'image_name',
        'watermark',
        'border',
        'hand',
        'life',
        'reserved',
        'release_date',
        'starter',
        'rulings',
        'foreign_names',
        'printings',
        'original_text',
        'original_type',
        'legalities',
    ];

    public function set() : BelongsTo
    {
        return $this->belongsTo(Set::class, 'set_id', 'id');
    }

    public function setManaCostAttribute($value)
    {
        if ($value !== null) {
            $this->attributes['mana_cost'] = json_encode($value);
        }
    }

    public function getManaCostAttribute($value)
    {
        return json_decode($value);
    }

    public function getLayoutAttribute()
    {
        // flip, double-faced, token, plane, scheme, phenomenon, leveler, vanguard, meld
        if (strpos($this->name, '//')) {
            return 'split';
        }
        return 'normal';
    }

    /**
     * Get the old mtgimage.com image name for a card.
     *
     * @return string
     */
    public function imageName() : string
    {
        return trim(strtolower(
            preg_replace(
                "/[^a-zA-Z0-9]+/",
                "",
                $this->name
            )
        ));
    }
}
