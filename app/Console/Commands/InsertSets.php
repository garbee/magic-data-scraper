<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\InsertSets as Service;

class InsertSets extends Command
{
    /** @var Service */
    private $service;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insertSets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @param Service $service
     *
     */
    public function __construct(Service $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $generator = $this->service->handle();
        $count = 0;
        foreach ($generator as $item) {
            if ($count === 0) {
                $progress = $this->output->createProgressBar($item);
                $count += 1;
                continue;
            }
            $progress->advance(1);
        }
        $progress->finish();
        $this->output->newLine(2);
        $this->info('All set data inserted.');
    }
}
