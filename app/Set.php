<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Set extends Model
{
    public $incrementing = false;

    protected $table = 'sets';

    protected $fillable = [
        "name",
        "code",
        "gatherer_code",
        "old_code",
        "magic_cards_info_code",
        "release_date",
        "border",
        "type",
        "block",
        "online_only",
        "booster",
        "cards",
    ];

    protected $dates = ['release_date'];

    public function cards() : HasMany
    {
        return $this->hasMany(Card::class, 'set_id', 'id');
    }

    public function setBoosterAttribute(array $value = null)
    {
        $this->attributes['booster'] = isset($value) ? json_encode($value) : null;
    }

    public function getBoosterAttribute($value)
    {
        return isset($value) ? json_decode($value) : null;
    }
}
