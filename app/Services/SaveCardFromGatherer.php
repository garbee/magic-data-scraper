<?php

namespace App\Services;

use App\Set;
use App\Card;
use MtgJson\Scraper\Services\RetrieveCard;

class SaveCardFromGatherer
{
    /** @var RetrieveCard */
    private $retrieve;

    /** @var Set */
    private $sets;

    /** @var Card */
    private $cards;

    /**
     * SaveCardFromGatherer constructor.
     * @param RetrieveCard $retrieve
     * @param Set $sets
     * @param Card $cards
     */
    public function __construct(RetrieveCard $retrieve, Set $sets, Card $cards)
    {
        $this->retrieve = $retrieve;
        $this->sets = $sets;
        $this->cards = $cards;
    }

    public function handle(string $cardName, string $multiverseId, string $setCode) : Card
    {
        /** @var Set $set */
        $set = $this->sets->newQuery()->where('code', '=', $setCode)->firstOrFail();
        $output = $this->retrieve->handle($cardName, $multiverseId);
        /** @var Card $card */
        $card = $this->cards->newQuery()->firstOrNew(['multiverseid' => $multiverseId]);
        $card->set()->associate($set);

        $card->name = $output->name;
        $card->mana_cost = $output->manaCost;
        $card->converted_mana_cost = $output->convertedManaCost;
        $card->number = $output->number;
        $card->artist = $output->artist;
        $card->power = $output->power;
        $card->toughness = $output->toughness;
        $card->loyalty = $output->loyalty;
        $card->flavor_text = $output->flavor;
        $card->rules_text = $output->text;
        $card->type = $output->type;
        $card->rarity = $output->rarity;

        $card->save();

        return $card;
    }
}
