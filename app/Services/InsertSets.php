<?php

namespace App\Services;

use App\Set;
use Carbon\Carbon;

class InsertSets
{
    public function handle()
    {
        $json = json_decode(file_get_contents(resource_path('data/sets.json')));
        yield count($json);
        foreach ($json as $data) {
            yield 1;
            $set = (new Set)->newQuery()->where('code', '=', $data->code)->first();
            if (!$set) {
                $set = new Set;
                $set->code = $data->code;
            }
            $set->name = $data->name;
            $set->gatherer_code = isset($data->gathererCode) ? $data->gathererCode : null;
            $set->magic_cards_info_code = $data->magicCardsInfoCode ?? null;
            $set->block = $data->block ?? null;
            $set->booster = $data->booster ?? null;
            $set->border = $data->border ?? null;
            $set->type = $data->type ?? null;
            $set->online_only = $data->onlineOnly ?? false;
            $set->old_code = $data ->oldCode ?? null;
            $set->tcgplayer_url_pattern = $data->tcgplayerUrlPattern ?? null;
            if ($data->releaseDate) {
                $releaseDate = explode(
                    '-',
                    explode(
                        ' ',
                        $data->releaseDate
                    )[0]
                );
                $set->release_date = Carbon::createFromDate(
                    $releaseDate[0] ?? null,
                    $releaseDate[1] ?? null,
                    $releaseDate[2] ?? null
                )->toDateString();
            }
            $set->save();
        }
    }

    public function handleAndReturn()
    {
        $generator = $this->handle();
        foreach ($generator as $item) {}
        return $generator->getReturn();
    }
}
