<?php

namespace App\Providers;

use App\Set;
use App\Card;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Set::creating(function (Set $set) {
            if (!$set->id) {
                $set->id = Uuid::uuid4();
            }
        });
        Card::creating(function (Card $card) {
            if (!$card->id) {
                $card->id = Uuid::uuid4();
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
