<?php

namespace App\Jobs;

use App\Set;
use Illuminate\Foundation\Bus\DispatchesJobs;
use MtgJson\Scraper\Services\RetrieveSetIds as Service;

class RetrieveSet extends Job
{
    use DispatchesJobs;

    /** @var Set */
    private $sets;

    /**
     * Create a new job instance.
     *
     * @param Set $sets
     */
    public function __construct(Set $sets)
    {
        $this->sets = $sets;
    }

    /**
     * Execute the job.
     *
     * @param string $setCode The set code to retrieve card ids for.
     * @return void
     */
    public function handle(string $setCode)
    {
        $set = $this->sets->newQuery()->where('code', '=', $setCode)->firstOrFail();
        $service = app(Service::class);
        $items = $service->handle($set->name);
        foreach ($items as $name => $multiverseId) {
            $this->dispatch(new RetrieveCard($setCode, $name, $multiverseId));
        }
    }
}
