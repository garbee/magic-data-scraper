<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\SaveCardFromGatherer as Service;

class RetrieveCard extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /** @var string */
    private $setCode;

    /** @var string */
    private $cardName;

    /** @var string */
    private $multiverseId;

    /**
     * RetrieveCard constructor.
     * @param string $setCode
     * @param string $cardName
     * @param string $multiverseId
     */
    public function __construct(string $setCode, string $cardName, string $multiverseId)
    {
        $this->setCode = $setCode;
        $this->cardName = $cardName;
        $this->multiverseId = $multiverseId;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /** @var Service $service */
        $service = app(Service::class);
        $service->handle($this->cardName, $this->multiverseId, $this->setCode);
    }
}
