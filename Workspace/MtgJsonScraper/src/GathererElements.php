<?php

namespace MtgJson\Scraper;

class GathererElements
{
    const PAGINATIONCONTAINER = '.paging';
    const ITEMLINK = '.nameLink';

    const BASEURL = 'http://gatherer.wizards.com';
    const CARDPAGEURL = self::BASEURL . '/Pages/Card/Details.aspx?multiverseid=';
    const CARDASPRINTEDPAGEURL = self::BASEURL . '/Pages/Card/Details.aspx?printed=true&multiverseid=';
    const CARDSETSANDLEGALITYURL = self::BASEURL . '/Pages/Card/Printings.aspx?multiverseid=';
    const CARDALTLANGUAGESURL = self::BASEURL . '/Pages/Card/Languages.aspx?multiverseid=';

    const CARDDATACONTAINER = '.cardComponentContainer';
    const BASEID = '#ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_';
    const NAME              = self::BASEID . 'nameRow';
    const MANACOST          = self::BASEID . 'manaRow';
    const CONVERTEDMANACOST = self::BASEID . 'cmcRow';
    const TYPE              = self::BASEID . 'typeRow';
    const TEXT              = self::BASEID . 'textRow';
    const FLAVOR            = self::BASEID . 'flavorRow';
    const POWERANDTOUGHNESS = self::BASEID . 'ptRow';
    const RARITY            = self::BASEID . 'rarityRow';
    const COLLECTORNUMBER   = self::BASEID . 'numberRow';
    const ARTIST            = self::BASEID . 'artistRow';

    const TABLECLASS = '.cardList';
}
