<?php

namespace MtgJson\Scraper\Services;

use Exception;
use Goutte\Client;
use MtgJson\Scraper\GathererData;
use Illuminate\Support\Collection;
use MtgJson\Scraper\GathererElements;
use Symfony\Component\DomCrawler\Crawler;

class RetrieveCard
{
    /** @var  Client */
    private $client;

    /** @var string */
    private $cardName;

    /**
     * RetrieveCard constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function handle(string $cardName, string $multiverseId) : GathererData
    {
        $this->setCardName($cardName);
        $crawler = $this->client
            ->request('GET', GathererElements::CARDPAGEURL . $multiverseId)
            ->filter(GathererElements::CARDDATACONTAINER)
            ->reduce(function (Crawler $item) {
                $nameContainer = $item->filter(GathererElements::NAME);
                if ($nameContainer->count() === 0) {
                    return false;
                }
                return trim($nameContainer->filter('.value')->text()) === $this->cardName;
            });

        if ($crawler->count() !== 1) {
            throw new Exception('Card data not found for: ' . $multiverseId);
        }

        $output = new GathererData();
        $output->name = $this->filterCrawlerBy($crawler, GathererElements::NAME);
        $output->manaCost = $this->getManaCost($crawler);
        $output->convertedManaCost = $this->filterCrawlerBy($crawler, GathererElements::CONVERTEDMANACOST);
        $output->type = $this->filterCrawlerBy($crawler, GathererElements::TYPE);
        $output->rarity = $this->filterCrawlerBy($crawler, GathererElements::RARITY);
        $output->flavor = $this->filterCrawlerBy($crawler, GathererElements::FLAVOR);

        if ($output->flavor === '') {
            $output->flavor = null;
        }

        $output->number = $this->filterCrawlerBy($crawler, GathererElements::COLLECTORNUMBER);

        if ($output->number === '') {
            $output->number = null;
        }

        $output->text = $this->getText($crawler);

        if ($output->text === '') {
            $output->text = null;
        }

        $this->checkPowerAndToughness($crawler, $output);
        $output->artist = $this->filterCrawlerBy($crawler, GathererElements::ARTIST);

        return $output;
    }

    private function getText(Crawler $crawler) : string
    {
        $text = '';
        $crawler
            ->filter(GathererElements::TEXT)
            ->filter('.cardtextbox')
            ->each(function (Crawler $item) use (&$text) {
                $markup = $item->html();
                $details = collect(explode('<img', $markup))
                    ->map(function ($item) {
                        return explode('>', $item);
                    })
                ->flatten();
                /** @var Collection $details */
                $details->each(function ($item) use (&$text) {
                    $more = [];
                    $alt = preg_match('/(alt)=("[^"]*")/i', $item, $more);
                    if ($alt === 0) {
                        // Append the text itself rebuilding broken italics.
                        $text .= str_replace('<i', '<i>', str_replace('</i', '</i>', $item));
                    } else {
                        // Append the mana cost found by the alt attribute.
                        $text .= '{' . str_replace('"', '', $more[2]) . '}';
                    }
                });
                // Add two newlines for legible spacing between rule blocks.
                $text .= "\n\n";
            });

        return $text;
    }

    private function checkPowerAndToughness(Crawler $crawler, GathererData &$output)
    {
        $node = $crawler->filter(GathererElements::POWERANDTOUGHNESS);

        if ($node->count() === 0) {
            return;
        }

        $label = trim($node->filter('.label')->text());

        switch ($label) {
            case 'P/T:':
                $data = explode('/', trim($node->filter('.value')->text()));
                $output->power = trim($data[0]);
                $output->toughness = trim($data[1]);
                break;
            case 'Loyalty:':
                $output->loyalty = trim($node->filter('.value')->text());
                break;
        }
    }

    private function getManaCost(Crawler $crawler) : array
    {
        $cost = [];
        $crawler->filter(GathererElements::MANACOST)
            ->filter('img')
            ->each(function (Crawler $image) use (&$cost) {
                array_push($cost, '{' . $image->attr('alt') . '}');
            });

        return $cost;
    }

    private function filterCrawlerBy(Crawler $crawler, string $target) : string
    {
        $target = $crawler->filter($target);

        if ($target->count() === 0) {
            return '';
        }

        return trim($target->filter('.value')->text());
    }

    private function setCardName(string $name)
    {
        $this->cardName = trim($name);
    }
}
