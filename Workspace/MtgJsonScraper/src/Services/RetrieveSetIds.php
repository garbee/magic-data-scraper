<?php

namespace MtgJson\Scraper\Services;

use Goutte\Client;
use Illuminate\Support\Collection;
use MtgJson\Scraper\GathererElements;
use Symfony\Component\DomCrawler\Crawler;

class RetrieveSetIds
{
    /** @var Client */
    private $client;

    /**
     * RetrieveSetIds constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function handle(string $setName) : array
    {
        /** @var Crawler $crawler */
        $crawler = $this->client->request(
            'GET',
            'http://gatherer.wizards.com/Pages/Search/Default.aspx?output=checklist&sort=cn+&set=[%22' .
            urlencode($setName) .
            '%22]'
        );
        $pages = $crawler
            ->filter(GathererElements::PAGINATIONCONTAINER)
            ->first()
            ->filter('a[href]')
            ->reduce(function (Crawler $crawler) {
                return strpos($crawler->attr('href'), '?page=');
            });
        $output = new Collection();
        if ($pages->count() === 0) {
            $output->push($this->getLinks($crawler));
        } else {
            $pages->each(function (Crawler $page) use ($output) {
                $output->push($this->getLinksFromPage($page->link()->getUri()));
            });
        }

        return $output
            ->collapse()
            ->map(function ($item) {
                return [array_keys($item)[0] => trim(str_replace(
                    GathererElements::CARDPAGEURL,
                    "",
                    array_values($item)[0]
                ))];
            })
            ->collapse()
            ->toArray();
    }

    protected function getLinks(Crawler $crawler) : array
    {
        $links = [];
        $crawler->filter(GathererElements::ITEMLINK)->each(function (Crawler $item) use (&$links) {
            array_push($links, [$item->text() => $item->link()->getUri()]);
        });
        return $links;
    }

    protected function getLinksFromPage(string $url) : array
    {
        $crawler = $this->client->request('GET', $url);
        return $this->getLinks($crawler);
    }
}
