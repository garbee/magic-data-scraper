<?php

namespace MtgJson\Scraper;

class GathererData
{
    /** @var string */
    public $name;

    /** @var string */
    public $multiverseId;

    /** @var array */
    public $manaCost;

    /** @var integer */
    public $convertedManaCost;

    /** @var string */
    public $type;

    /** @var string */
    public $rarity;

    /** @var string */
    public $text;

    /** @var string */
    public $flavor;

    /** @var string */
    public $number;

    /** @var string */
    public $artist;

    /** @var string */
    public $power;

    /** @var string */
    public $toughness;

    /** @var int */
    public $loyalty;
}
