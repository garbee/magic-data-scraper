<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sets', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('code')->comment('The unique set code identifier.')->unique();
            $table->string('name')->comment('The full name of the set');
            $table->string('gatherer_code')
                ->comment('The code that Gatherer uses for the set. Only present if different than \'code\'')
                ->nullable();
            $table->string('old_code')
                ->comment(
                    'An old style code used by some Magic software. ' .
                    'Only present if different than \'gathererCode\' and \'code\''
                )
                ->nullable();
            $table->string('magic_cards_info_code')
                ->comment(
                    'The code that magiccards.info uses for the set. Only present if magiccards.info has this set'
                )
                ->nullable();
            $table->string('tcgplayer_url_pattern')
                ->comment(
                    'The TCGPlayer URL identifier for their category pages.'
                )
                ->nullable();
            $table->date('release_date')
                ->comment(
                    'When the set was released (YYYY-MM-DD). For promo sets, the date the first card was released.'
                )
                ->nullable();
            $table->string('border')
                ->comment(
                    'The type of border on the cards, either "white", "black" or "silver"'
                )
                ->nullable();
            $table->string('type');
            $table->string('block')
                ->comment('The block the set belongs to')
                ->nullable();
            $table->boolean('online_only')->default(false);
            $table->jsonb('booster')->nullable();
            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sets');
    }
}
