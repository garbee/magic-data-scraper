<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('old_id')
                ->comment('The old MTGJSON id for a card. Null for things created after the json import.')
                ->nullable();
            $table->string('name');
            $table->string('layout')->nullable();
            $table->jsonb('names')->nullable();
            $table->string('mana_cost')->nullable();
            $table->string('converted_mana_cost')->default(0);
            $table->jsonb('colors')->nullable();
            $table->jsonb('color_identity')->nullable();
            $table->string('type');
            $table->jsonb('types')->nullable();
            $table->jsonb('supertypes')->nullable();
            $table->jsonb('subtypes')->nullable();
            $table->string('rarity');
            $table->text('rules_text')->nullable();
            $table->text('flavor_text')->nullable();
            $table->string('artist')->nullable();
            $table->string('number')->nullable();
            $table->string('power')->nullable();
            $table->string('toughness')->nullable();
            $table->integer('loyalty')->nullable();
            $table->string('multiverseid')->nullable()->unique();
            $table->jsonb('variations')->nullable();
            $table->string('imageName')->nullable();
            $table->string('watermark')->nullable();
            $table->string('border')->nullable();
            $table->boolean('timeshifted')->default(false);
            $table->string('hand')->nullable();
            $table->string('life')->nullable();
            $table->boolean('reserved')->default(false);
            $table->date('release_date')->nullable();
            $table->boolean('starter')->default(false);
            $table->jsonb('rulings')->nullable();
            $table->jsonb('foreign_names')->nullable();
            $table->jsonb('printings')->nullable();
            $table->text('original_text')->nullable();
            $table->string('original_type')->nullable();
            $table->jsonb('legalities')->nullable();
            $table->string('source')->nullable();
            $table->uuid('set_id')->nullable();
            $table->timestamps();
            $table->primary('id');
            $table->foreign('set_id')
                ->references('id')
                ->on('sets')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cards');
    }
}
